from django.shortcuts import render
from .models import Feeling
from .forms import FeelingForm
from authapp.models import UserProfile
from django.http import JsonResponse
from django.core import serializers
import json

# Create your views here.
def info(request):
    if request.user.is_authenticated:
        formFeeling = FeelingForm(request.POST or None)
        return render(request, 'info-corona-feel.html', {'formFeeling': formFeeling})
    return render(request, 'info-corona.html')

def info_data(request):
    if request.user.is_authenticated:
        u = UserProfile.objects.get(user=request.user)
        if request.method == 'POST':
            f = request.POST.get('feeling')
            Feeling.objects.create(user = u, feeling = f)
        
        feelings = Feeling.objects.all()
        obj = serializers.serialize('json', feelings)
        datas = json.loads(obj)
        for data in datas:
            id = data['fields']['user']
            data['fields']['user'] = UserProfile.objects.get(pk=id).full_name
    return JsonResponse(datas, safe=False)