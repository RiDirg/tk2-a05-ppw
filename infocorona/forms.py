from django import forms
from .models import Feeling

class FeelingForm(forms.ModelForm):
    
    class Meta:
        model = Feeling
        fields = ('feeling',)
        labels = {
            'feeling' : ''
        }
        widgets = {
            'feeling' : forms.Textarea(attrs={
                'id': 'feeling',
                'placeholder': ' I feel ...',
                'style': 'width: 100% ; background-color:#F7FBE1; border:0; font-family: "Poppins Light"; padding:5px; ',
                'cols':5, 'rows':5,
                },)
        }
