from django.urls import path

from . import views

app_name = 'infocorona'

urlpatterns = [
    path('info-corona/', views.info, name='info-new-user'),
    path('info-corona/data/', views.info_data, name='info-data'),
]