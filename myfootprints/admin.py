from django.contrib import admin

# Register your models here.
from .models import MyFootPrints

admin.site.register(MyFootPrints)