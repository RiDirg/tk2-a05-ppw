from django import forms

from .models import MyFootPrints

class MyFootPrintsForm(forms.ModelForm):
    place = forms.CharField()
    date = forms.DateField()
    desc = forms.CharField()
    
    class Meta:
        model = MyFootPrints
        fields = ('place', 'date', 'desc')