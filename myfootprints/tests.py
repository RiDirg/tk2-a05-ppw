from django.test import TestCase, Client
from .models import MyFootPrints

# Create your tests here.

class UnitTest(TestCase):
    def test_url_is_valid(self):
        response = Client().get('/my-footprints')
        self.assertEqual(response.status_code, 301)
        
    def test_myfootprints_create_object(self) :
        place = MyFootPrints.objects.create(place="PIM",date="2020-05-28",desc="keluar rumah")
        count = MyFootPrints.objects.all().count()
        self.assertEqual(count, 1)