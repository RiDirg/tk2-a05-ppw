from django.urls import path

from . import views

app_name = 'authapp'

urlpatterns = [
    path('signup/', views.signup, name='signup'),
    path('signin/', views.signin, name='signin'),
    path('logout/', views.logoutuser, name='logout'),
    path('profile/', views.profile, name='profile')
]
