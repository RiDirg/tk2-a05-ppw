from django.urls import path

from . import views

app_name = 'covid19App'

urlpatterns = [
    path('corona-cases/', views.index_cases, name='index-cases'),
]